[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_29]]/[[ES|Nouveau_Companion_29-es]]/[[FR|Nouveau_Companion_29-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 28 Octobre 2007


### Introduction

Et on y retourne : préparez-vous pour l'édition numéro 29 du TiNDC. Cette version est un peu mise à jour par rapport à la version de Phoronic, puis que j'avais oublié quelque chose d'important. 

Quand nous parlions de l'infrastructure du projet dans les précédentes éditions, c'était généralement des mauvaises nouvelles (les scripts qui ne marchaient pas, le déplacement vers fd.o, etc.). Enfin, nous pouvons dire quelque chose de positif : Nous avons des versions HTML [ndt: des logs IRC] avec des liens et les pseudo en couleur grête à un script de MJules. Cette version a été vien reçue et permet même de mettre des tags afin de pouvoir faire des liens vers une ligne précise dans le log (en entrant '==== (topic)'). Vous pouvez voir quels topics ont été taggés ici : [[http://people.freedesktop.org/~ahuillet/irclogs/tags_index.htm|http://people.freedesktop.org/~ahuillet/irclogs/tags_index.htm]] 

Mais le script a donné quelques faux positifs, donc pachi l'a amélioré. Le script est maintenant capable d'ajouter des ancres HTML automatiquement basé sur les _timestamp_. Vous pouvez trouver les deux scripts, les feuilles de style et les logs colorés sur [[http://people.freedesktop.org/~ahuillet/irclogs|http://people.freedesktop.org/~ahuillet/irclogs]] .  

Cette option pourrait devenir pratique pour les TiNDC. Faites-nous savoir si vous aimez ça ou pas. 

De plus, nous avons maintenant une page où les demandes de tests importantes sont annoncées. Donc si vous voulez nous aider et que vous ne voulez pas coder, jetez un œuil là : [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] . 

Un dernier sujet avant que nous commencions le rapport de progression lui-même. La question a été posée, pourquoi y a-t'il un délai si long entre la publication sur phoronix.com et sur le Wiki. C'est simple : 

* Une période de répit de quelques heures 
* Des _timezones_ différentes (ce qui va bien avec la raison 1) 
* Et cette-fois-ci à cause de soucis de la vraie vie, je n'ai pas pu avoir accès à mon PC avant samedi minuit (CEST). À ce moment KMeyer était déjà abandonné. 

### Statut actuel

ahuillet a ajouté un patch pour améliorer les performances sur NV04. Le patch mettait en cache les appels à BLIT_OPERATION et RECT_OPERATION. Puisque ce sont des méthodes logicielles, les appeler trop souvent ralentit considérablement le système. ([[http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=6d8caf5e0dd915809152c52c2c56a39d76e2ed8c|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=6d8caf5e0dd915809152c52c2c56a39d76e2ed8c]]) Malheureusement, un test a révélé plus tard que ce patch cassait le rendu 2D si une application SDL changeait de mode d'affichage 

À la fin de la dernière édition, nous avons vu ahuillet, pmdata et p0g se battre avec les périls du texturage et du coloriage des triangles et des quads sur NV1x. Les résultats variaient énormément (depuis la modification de la couleur de quads noirs, jusqu'à seulement la partie de la texture affichée (1 Texel)), choisissez l'erreur que vous voulez et nos développeurs peuvent vous dire qu'ils l'ont déjà expérimentée. 

Mais finalement ils y sont arrivés, il manquait le bit de texturing dans le code d'initialisation. 

P0g a travaillé un peu plus sur le _blending_ et la transparence sur NV1x. Ses premiers essais laissaient des bords noirs autour des icônes au lieu du fond. Il a fait quelques recherches et a trouvé la cause : la transparence n'était pas activée, donc il a rapidement sorti un patch. [[!img http://peter.whoei.org/nouveau/icons-semi-working.png] 

(IRC: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-23#0022|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-23#0022]]): [[http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=cf053adacabaee887ecedaa9967b07b3185095b5|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=cf053adacabaee887ecedaa9967b07b3185095b5]] 

Un autre problème résolu était que les textures _non power of two_ (textures avec une largeur / hauteur qui n'est pas une puissance de 2) devaient avoir une taille paire. Les tailles impaires sont arrondies. 

Bien que ces résultats soient essentiels pour avoir EXA sur NV1x (et la rumeur dit que cette texture pourrait être utilisée en 3D plus tard), il manque encore quelques parties importantes : _[[PictOps|PictOps]]_ et masquage. 

Juste avant la _deadline_ éditoriale, ahuillet a fait marcher le masquage et un peu des _[[PictOps|PictOps]]_. Puisque NV1x ne supporte pas les shaders, il a utilisé les combinateurs de registres (voir insert). Cependant, le _[[PictOp|PictOp]]_ le plus important, A8 + A8 (Alpha 8 bits) n'était pas encore implémenté. La raison est que NV1x ne supporte pas A8 comme destination. Tant que cela ne sera pas implémenté, on ne sentira pas que EXA est réellement accéléré sur NV1x. [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-25.htm#1545|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-25.htm#1545]] 

Donc ahuillet a annoncé la victoire par rapport à marcheu et jkolb dans la course de quelle carte aurait EXA qui marche en premier : NV1x (ahuillet, p0g) ou NV3x (jkolb, marcheu). 

Malheureusement, marcheu s'est aperçu quelques heures plus tard que le EXA des NV1x ne pouvait pas gérer le XBGR, ce qui (d'après ses chiffres) voudrait dire que la plupart des appels EXA seraient gérés de manière logicielle. ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0247|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0247]]) 

Donc quelques heures plus tard il a soumit une correction pour EXA sur NV3x qui réglait tous les problèmes connus. _A8 op A8_ a été réglé plus tard à travers une mise à jour DRM et marche actuellement, et la répétition de zones de 1x1 marche aussi bien. Ce qui manque maintenant pour que EXA marche complètement est la répétition de zones de taille supérieure à 1x1, mais ce n'est pas beaucoup utilisé et demanderait l'utilisation d'un shader pour calculer le _wrapping_ des coordonnées. Malheureusement, les vraies cartes NV30 ne marchent pas actuellement puisque l'initialisation dans le DRM semble incorrect (GeForce 5800 et Quadro FX1000). [[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0451|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0451]]) 

Insert: Combinateurs de registres 

---

 Il n'y a aucun shader sur NV1x. Donc afin d'accomplir la même chose que sur les cartes modernes, il faut utiliser des combinateurs de registre (ou de texture). 

Bien que les combinateurs de registres et les combinateurs de textures marchent différemment dans les opérations 3D, pour notre travail 2D actuel, ils peuvent être vus d'une manière similaire (voire même égale). Les combinateurs de registre sont câblés matériellement dans l'étape de texturing du matériel. Donc quand vous configurez cette partie de la carte, vous récupérez automatiquement le comportement par défaut. 

Maintenant, quel est de comportement par défaut? Si vous dessinez un pixel sur l'écran il est modifié par la couleur du vertex. Si vous voulez le moduler par quelque chose d'autre (par exemple l'alpha d'une autre texture), vous devez "juste" configurer le combinateur de registres afin qu'il implémente l'opération _IN_ qui est requise pour le masquage. 

---

 

Après que ahuillet a annoncé avoir EXA qui marchait presque, pq a commencé à tester l'EXA du NV10 sur sa NV2x. Peu après, il a découvert que _[[DownloadFromScreen|DownloadFromScreen]]_ (abbrévié en _DFS_) ne marchait pas. Par dichotomie il est revenu à la dernière modification de l'api binaire et a remarqué que DFS ne marchait déjà pas. 

Jkolb et marcheu on travaillé sur NV30. Quelques problèmes ont été résolus : 

* Le positionnement et la taille EXA des rectangles était parfois fausse. 
* Les coordonnées EXA n'étaient pas calculées correctement. Ils étaient modifiés pour être dans la plage [0..1] au lieu de [0..largeur]. Résolu par jkolb. 
À ce moment, le driver était incapable de faire la traspanrence, par exemple pour les icônes. Toutes les parties transparentes étaient dessinées en noir. Après que jkolb ait tenté sa chance et ait perdu contre cette carte vraiment trop têtue, marcheu a tenté sa chance et a été chanceux<sup>W</sup>W montré son professionnalisme. 

À la base, le NV10 n'est pas très loin derrière le NV3x. Il lui manque juste le _[[PictOp|PictOp]]_ A8+A8. 

Bien que darktama ait été discret durant les dernières semaines, il n'était pas paresseux. Il a travaillé sur un TTM pour le NV4x et un prototype de Gallium (juste quelques recherches, ce code ne sera pas publié tel quel). Après avoir parlé avec marcheu à propos des pour et des contre de l'architecture, ils sont tous les deux arrivés à la conclusion que Gallium était la voie à suivre. 

Pendant que marcheu et darktama arrivaient à une décision, pmdata voulait commencer à coder la fonction DRI sur NV1x, mais Marcheu lui a demandé de ne pas le faire. Raisonnement : 

* Pour l'instant nous voulons avoir EXA qui marche, avec tous les progrès mentionnés ci-dessus, ça ne prendra plus beaucoup de temps. 
* Gallium est différent du DRI actuel, notamment pour les NV2x et les cartes plus récentes. Dravailler sur DRI serait une perte de temps. 
* Marcheu est confiant qu'il n'y aura pas beaucoup de problème à faire marcher les cartes >=NV3x avec Gallium, mais les cartes plus jeunes auront besoin de quelques tricheries (que SuperMarcheu va fournir :) ) 
Voir : [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-19.htm#2025|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-19.htm#2025]] 

Avec cela en tête, marcheu a arrêté tous ses autres projets en rapport avec Nouveau et a commencé à ajouter une infrastructure générique pour Gallium qui va permettre aux anciennes cartes avec des _pipelines_ inexistantes ou fixes d'offrir une accélération. Pour Nouveau cela serait toutes les cartes < NV30. 

Mais ce travail est générique, il sera utile pour toutes les cartes à qui il manque les pipelines des _vertex_ et _pixel shaders_ (par exemple les anciennes radeon). Après cela, pmdata pourra enfin commencer à travailler sur DRI. 

Stillunknown a été harcelé par KoalaBR jusqu'à ce qu'il abandanne et qu'il propose de (essayer de) faire marcher le changement de mode sur NV50 / G84. Dans un acte de revenche, il a engagné KoalaBR comme testeur. Les premiers tests prudents pour essayer de faire marcher le changement de mode n'ont produit aucun changement par rapport à la situation actuelle. Donc nous ne pouvons toujours pas revenir au mode texte depuis X11. 

Quelques autres sujet qui valent le coût d'être évoqués : 

* Le patch de documentation de hkBst pour rules-ng des registres en rapport avec Xv ont été incorporés par pq 
* [[IronPeter|IronPeter]] et son travail sur la PS3 progresse régulièrement 
* À cause du problème dont nous avions parlé dans l'édition précédentes, les _notifiers_ sont maintenant toujours créés en mémoire PCI. 
* [[StillUnknown|StillUnknown]] a ajouté quelques registres de contrôle vidéo à rules-ng pour NV50 
* pmdata travaille pour trouver quels formats de texture sont supportés sur NV1x et il a aidé ahuillet et p0g avec leurs problèmes de NV1x 
* le DRM a eu une correction qui évite à la carte de se planter en cas d'erreur 
* careym a corrigé quelques bugs OpenGL dans renouveau 
* chowmeined a corrigé quelques changement de mode sur NV5x, ce qui a provoqué quelques lignes roses sur le côté droit et en bas. 
[[MmioTracing|MmioTracing]] sur des NV50 pourrait bloquer violemment la machine. Ça a été découvert par KoalaBR (mais il n'a pas été capable de le reproduire plus tard). PQ peut le reproduire et essaye d'enquêter, mais il serait heureux d'avoir quelqu'un avec une NV50 qui puisse avoir le crash et aurait un port série connecté à un terminal série (un autre ordinateur par exemple). La tâche ne demande aucune connaissance en programmation, mais il faut savoir configurer et installer un kernel modifié, appliquer des patchs et tolérer beaucoup de crashs et de reboots. Cela pourrait être un long combat, donc cette personne devrait être capable de rester pour au moins quelques semaines. 


### Aide requise

Stillunknown cherche des possesseurs de 7xx0 qui pourraient lancer quelques tests pour lui. Il cherche également des personnes avec du _dual head_ (quelle que soit la combinaison de VGA / DVI) qui voudraient tester le changement de mode / randr1.2. 

De plus, jetez un œuil à la page « Tester wanted » pour les demandes qui arrivent entre nos éditions. [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

[[<<< Édition précédente|Nouveau_Companion_28-fr]] | [[Édition suivante >>>|Nouveau_Companion_30-fr]] 
