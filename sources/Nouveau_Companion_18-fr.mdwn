[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_18]]/[[ES|Nouveau_Companion_18-es]]/FR/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 25 avril 2007

[!] La traduction est terminée, mais une relecture ne serait pas de refus 
### Introduction

Il n'y a pas grand chose à rapporter des deux dernières semaines, puisque nous attendons toujours que le gestionnaire de mémoire TTM apparaisse en _mainstream_ sous une forme utilisable. Sans cela, nous sommes tout simplement bloqués. Utiliser l'ancien gestionnaire mémoire fait maison conduisait à la duplication de travail inutile, alors qu'attendre ne nous amènera nulle part. 

Google a annoncé ses choix parmi les projet du _Summer of Code_. Malheureusement, Nouveau n'a pas eu une seule place. Malgré cela, Egbert Eich de Xorg est arrivé dans notre canal IRC et a annoncé le Xorg "Vacation of Code", dont le but est de venir en remplacement pour les projet Xorg rejetés, cela vaut donc aussi pour les notres.  Les détails sont encore maigres, puisque Xorg essaye cela pour la première fois, et les problèmes seront résolus lorsqu'ils apparaîtront. Cependant, tous les étudiants qui ont proposé des projets Xorg finalement rejetés ont déjà été contactés par Xorg et ont été informés. 

À la date du 20/04/2007, nous avons un seul projet qui a été accepté : le support de Xv pour Nouveau par Ahuillet. De plus, le projet de créer un nouvel algorithme de compression de texture S3TC (qui est brevetée) a été accepté également. Cela sera bénéfique pour tous les drivers, et pas seulement nouveau. Regardez l'annonce officielle ici : [[http://summer.cs.pdx.edu/|http://summer.cs.pdx.edu/]] 

Félicitations à tous ceux qui sont impliqués. Et à ceux dont les propositions ont été rejetées deux fois : Nous sommes désolés! Cependant, nous souhaitons réellement que vous travailliez tout de même sur Nouveau, si vous trouvez le temps. 

En ce qui concerne l'argent des dons : le transfert est toujours en cours à cause d'un problème de notre côté. Si marcheu acceptait l'argent, nous aurions à payer une énorme quantité de taxes, nous avons donc besoin d'une association à but non lucratif pour gérer cela pour nous. Malheureusement, nous ne pouvons en créer et en gérer une (ce projet est seulement composé de volontaires travaillant sur leur temps libre), nous essayons donc d'être acceptés en tant que sous-projet d'une telle association et qu'elle gère l'argent pour nous. Cependant, ce processus avance très lentement. 


### État actuel

pq et z3ro ont continué leur travail sur l'amélioration du schema _rules-ng_ et ont commencé à l'implémenter pour tester quelques trucs. Une version préliminaire sur une trace mmio a produit environ 820 000 lignes converties / annotées. Puisque pq est en train de vérifier à la main chaque ligne, nous ne pouvons pas encore vous donner un résultat final (il a seulement commencé le 19/04/2007) :) 

jwstolk a commencé son travail pour trouver des séquences de code communes dans les dumps mmio. Cela pourrait mené à la création de macros, qui ne sont rien d'autre qu'un nom descriptif pour une longue séquence d'écriture de registres qui sont autrement incompréhensibles (incompréhensibles pour l'habitant standard du canal IRC, nos leaders sans peur désassemblent cela en mnémonique de tête :) ) 

De plus, headergen.py a aussi été fini par pq. Ce script crée un fichier header à partir de la base de donnée précédemment mentionnée,  incluant des #define des adresse, noms et valeurs possibles pour tous les registres connus. 

Pendant le weekend de pâques, hughsie et darktama one essayé de faire marcher la 7300 Go de hughsie (ahem). Après un travail basique concernant le _voodoo_, darktama a concocté un patch conduisant au succès. Cette carte est maintenant fonctionnelle, ceci incluant glxgears. 

Et maintenant, une liste des évènements intéressants du canal IRC, qui peuvent être résumés en une phrase : 

* Le script de KoalaBR pour télécharger, compiler et lancer renouveau a été mis à jour après retours utilisateurs. 
* Un script pour récupérer toutes les informations importantes à propos du système après un crash de nouveau est en train d'être construit et vérifié par les hautes instances du projet. 
* Aeichner a essayé de porter Nouveau sur *BSD, mais a arrêté à cause du fait que DRM sur BSD est en retard en ce qui concerne les fonctionnalités. 
* Darktama a un petit désassembleur qui décode en partie les valeurs du _context voodoo_. 
* austin1 a réussi à faire marcher glxinfo et glxgears sur sa NV42. 
Il semble que nous avons des problèmes pour les cartes < NV40. Nous obtenons souvent des interruptions PGRAPH_ERROR. On dirait que notre contexte d'initialisation n'est pas encore au points. Marcheu est en train d'étudier cela. 

stephan_2303 nous a aidé à faire marcher Nouveau sur une NV4B. Pendant le test de ses patches, il a été déduit que les 4 premiers bits de NV03_PFIFO_RAMHT définissent la taille des tables de hashage. Sur une cartes NVidia, chaque contexte a un objet assigné / créé pour lui. Pour trouver ces objets, une table de hashage est utilisée. Malheureusement un test important dans REnouveau a été désactivé un peu plus tôt cette année parce qu'il planté REnouveau fréquemment. Nous avons réactivé ce test (et pq a planté son système en le testant, on dirait qu'il va devoir comparer depuis le début les résultats de mmio-parse :) ) 

KoalaBR a effectué quelques tests avec quelques jeux 2D (_majesty_ et _Railroad Tycoon 2_) et a trouvé quelques problèmes pendant la mise à jour de l'écran / la fenêtre du jeu. Cela amenait à une corruption importante de l'affichage, sans que quoi que ce soit d'utile puisse être vu à l'écran. Une étude plus approfondie a montré que c'est le blending qui était en faute. Le désactiver ramena des résultats corrects. 

Celui qui s'est le plus accroché lors de ces compte-rendus est jb17some : après avoir suivi ses progrès tout le long des 2 dernières versions du TiNDC, il a finalement réussi à faire marcher le changement de contexte sur sa NV49. Le patch nécessaire était presque le même que pour la NV4B, excepté pour quelques petits ajustements. 

Encore plus de problèmes avec notre infrastructure : nous avons atteint la limite maximale pour l'espace de stockage des données sur [[SourceForge|SourceForge]]. Cela a corrompu nos uploads et nous avons donc du les désactiver. Si vous avez envoyer des _dumps_, c'est pour ça que vous ne voyez pas de mise à jour sur la page de JussiP. 

Vous vous rappelez de REnouveau, notre outil de reverse engineering? Après l'avoir négligé pendant longtemps, plusieurs choses sont arrivées récemment : 

* Script par KoalaBR pour télécharger, compiler et le lancer automatiquement. 
* Le test des registres intéressants a été réactivé (comme mentionné précédemment) 
* L'information de version est intégrée à la sortie 
En plus de cela, nous réfléchissons à la manière donc nous devons / pouvons séparer REnouveau en 2 programmes différents : un _dumper_ et un _parser_. Le _dumper_ ne ferait que sortir les informations binaires (au lieu de les afficher sous un format compréhensible comme maintenant), pendant que le _parser_ prendrait les données pour les transformer en quelque chose de compréhensible. Cela permettrait un schéma identique à ce que pq est en train d'implémenter pour les _dumps_ mmio. De plus, les _dumps_ générés seraient bien plus petits. 

La branche randr1.2 a fait l'objet d'attentions particulières pendant ces derniers jours. Tous les changements depuis la branche principale ont été fusionnées, et plusieurs ajouts ont été faits. Peu de temps après, pq a fait quelques tests avec sa configuration et a trouvé quelques problèmes d'affichage (mauvaises couleurs, écran décalé vers la droite). Ce qui est bien, cependant, c'est que Airlied a exactement les mêmes erreurs d'affichage sur sa configuration. Comparé aux tests précédents, le progrès est clairement visible. Mais le travail avance lentement et il reste beaucoup de travail à faire. 


### Aide requise

En mettant de côté les problèmes avec SourceForge, nous cherchons des _dumps_ de SLI et de 8x00. Si quelqu'un avec quelques connaissances en programmation et une 8x00 voulais bien nous aider, nous lui en serions reconnaissants. 

De plus, nous cherchons des _dumps_ MMio pour les cartes ne peuvent pas encore faire tourner glxgears correctement. 

Et si vous êtes la personne qui a réussi à faire marcher sa NV44 : nous sommes toujours en attente de votre patch (Je crois que votre surnom était "mg"). 

[[<<< Édition précédente|Nouveau_Companion_17-fr]] | [[Édition suivante >>>|Nouveau_Companion_19-fr]] 
