<link rel="stylesheet" href="index.css" type="text/css" />

<table class="boxes">
<tr>
<td colspan="2" class="wide">
<div class="box"> 

<h1>Nouveau: Accelerated Open Source driver for nVidia cards</h1>

<p>
The <b>nouveau</b> project aims to build high-quality, free/libre software drivers for [[nVidia cards|CodeNames]].  “Nouveau” [<i>nuvo</i>] is the French word for “new”. Nouveau is composed of a Linux kernel KMS driver (nouveau), Gallium3D drivers in Mesa, and the Xorg DDX (xf86-video-nouveau). The kernel components have also been ported to [[NetBSD]].
</p>

<h2>Current Status</h2>

<ul>
<li>2D/3D acceleration supported on all GPUs (except for [[GA10x|CodeNames#nv170familyampere]]); see [[FeatureMatrix]] for details.</li>
<li>Video decoding acceleration supported on most pre-Maxwell cards; see [[VideoAcceleration]] for details.</li>
<li>Support for manual performance level selection (also known as "reclocking") on [[GM10x Maxwell|CodeNames#nv110familymaxwell]], [[Kepler|CodeNames#nve0familykepler]] and [[Tesla G94-GT218|CodeNames#nv50familytesla]] GPUs. Available in <code>/sys/kernel/debug/dri/0/pstate</code></li>
<li>Little hope of reclocking becoming available for [[GM20x|CodeNames#nv110familymaxwell]] and newer GPUs as firmware now needs to be signed by NVIDIA to have the necessary access.</li>
</ul>

<h2>News</h2>

<ul>
<li>Jan, 2021: [[GA10x|CodeNames#nv170familyampere]] kernel mode setting support merged in Linux 5.11.</li>
<li>Jan, 2019: [[TU10x|CodeNames#nv160familyturing]] acceleration support (with redistributable signed firmware) merged in Linux 5.6.</li>
<li>Jan, 2019: Support for Turing merged into Linux 5.0.</li>
<li>Nov, 2018: Support for HDMI 2.0 high-speed clocks merged into Linux 4.20 (GM200+ hardware only).</li>
<li>Aug, 2018: Support for Volta merged into Linux 4.19.</li>
</ul>


</div>
</td>
</tr>

<tr>
<td>
<div class="box">

<h2>Software</h2>

<table class="versions" style="margin-left: 40px; border: 1px solid #eee; width: 80%;" cellspacing="0" cellpadding="3">
<tr><td>[[Linux Kernel|http://www.kernel.org/]]</td><td>[[git|https://github.com/skeggsb/linux/]]</td></tr>
<tr><td>libdrm</td><td>[[git|http://cgit.freedesktop.org/mesa/drm/]]</td></tr>
<tr><td>[[Mesa|http://www.mesa3d.org/]]</td><td>[[git|http://cgit.freedesktop.org/mesa/mesa/]]</td></tr>
<tr><td>xf86-video-nouveau</td><td>[[git|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/]]</td></tr>
</table>

</div>
</td>
<td>
<div class="box">

<h2>Contacting the Team</h2>

<ul>
<li>IRC: #nouveau on [[OFTC|https://oftc.net]]. [[Logs|https://people.freedesktop.org/~cbrill/dri-log/index.php?channel=nouveau]], or [[IrcChatLogs]]</li>
<li>Mailing lists: [[nouveau|https://lists.freedesktop.org/mailman/listinfo/nouveau]], [[dri-devel|https://lists.freedesktop.org/mailman/listinfo/dri-devel]], [[mesa-dev|https://lists.freedesktop.org/mailman/listinfo/mesa-dev]]</li>
</ul>

</div>
</td>
</tr>

<tr>
<td>
<div class="box">

<h2>Common Tasks</h2>

<ul>
<li>[[Installing Nouveau|InstallNouveau]]
<li>Multiple monitors: [[Randr12]] and [[MultiMonitorDesktop]]</li>
<li>Prime/Optimus: [[Optimus]]</li>
<li>[[VDPAU]]/[[XvMC]]: [[Video decoding acceleration|VideoAcceleration]]</li>
<li>[[KernelModuleParameters]]</li>
</ul>

</div>
</td>
<td>
<div class="box">

<h2>Get Involved</h2>

<ul>
<li>Join us on IRC</li>
<li>[[Become a developer|IntroductoryCourse]]</li>
<li>[[Run tests on your hardware|TestersWanted]]</li>
<!-- <li>Register as a tester</li> -->
<li>[[Donate hardware|HardwareDonations]]</li>
<li>[[Available tasks on Trello|https://trello.com/b/ZudRDiTL/nouveau]], [[CTS issues on Trello|https://trello.com/b/lfM6VGGA/nouveau-cts]], [[Old ToDo|ToDo]] list</li>
</ul>

</div>
</td>
</tr>

<tr>
<td>
<div class="box">

<h2>Report Bugs</h2>

<ul>
<li>[[TroubleShooting]]</li>
<li>[[Bugs]]</li>
<li>[[FAQ]]</li>
</ul>

</div>
</td>
<td>
<div class="box">

<h2>Development/Debugging</h2>

<ul>
<li>[[Development]] resources</li>
<li>[[envytools|https://github.com/envytools/envytools/]]: RE tools and docs</li>
</ul>

</div>
</td>
</tr>
</table>

<div class="boxes" style="text-align: center">
The content of this wiki is licensed under the [[MIT License|http://opensource.org/licenses/mit-license.php]] unless stated otherwise by the author of specific wiki pages.
</div>
